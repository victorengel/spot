//
//  LatestStanfordTVC.m
//  SPoT
//
//  Created by Victor Engel on 3/21/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "LatestStanfordTVC.h"
#import "FlickrFetcher.h"

@interface LatestStanfordTVC ()

@end

@implementation LatestStanfordTVC

-(void) viewWillAppear:(BOOL)animated
{
   [super viewWillAppear:animated];
   NSMutableArray *latestPhotos = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"recentPhotos"] mutableCopy];
   if ([latestPhotos count] > 10) {
      int highest = [latestPhotos count]-1;
      for (int index=highest; index>=10; index--) {
         [latestPhotos removeObjectAtIndex:index];
      }
   }
   self.photos = [latestPhotos copy];
   self.recentPhotos = latestPhotos;
   // Save the recent photos array to NSUserDefaults
   [[NSUserDefaults standardUserDefaults] setObject:self.recentPhotos forKey:@"recentPhotos"];
   [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
