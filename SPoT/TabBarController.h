//
//  TabBarController.h
//  SPoT
//
//  Created by Victor Engel on 3/31/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController

@end
