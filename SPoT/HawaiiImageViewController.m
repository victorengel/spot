//
//  HawaiiImageViewController.m
//  SPoT
//
//  Created by Victor Engel on 3/20/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "HawaiiImageViewController.h"

@interface HawaiiImageViewController ()

@end

@implementation HawaiiImageViewController

- (void)viewDidLoad
{
   [super viewDidLoad];
   self.imageURL = [[NSURL alloc] initWithString:@"http://images.apple.com/v/iphone/gallery/a/images/photo_3.jpg"];
	// Do any additional setup after loading the view.
}

@end
