//
//  StanfordTVC.m
//  SPoT
//
//  Created by Victor Engel on 3/21/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "StanfordTVC.h"
#import "FlickrFetcher.h"
#import "IViewController.h"

@interface StanfordTVC ()
@property NSString *selectTag;

@end

@implementation StanfordTVC

- (id)splitViewDetailWithBarButtonItem
{
   id detail = [self.splitViewController.viewControllers lastObject];
   if(![detail respondsToSelector:@selector(setSplitViewBarButtonItem:)] ||
      ![detail respondsToSelector:@selector(splitViewBarButtonItem)])
   {
      detail = nil;
   }
   return detail;
}
- (void)transferSplitViewBarButtonItemToViewController:(id)destinationViewController
{
   UIBarButtonItem *splitViewBarButtonItem = [[self splitViewDetailWithBarButtonItem] performSelector:@selector(splitViewBarButtonItem)];
   if (splitViewBarButtonItem)
   {
      [destinationViewController performSelector:@selector(setSplitViewBarButtonItem:)
                                      withObject:splitViewBarButtonItem];
   }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   NSMutableArray *latestPhotos = [self.recentPhotos mutableCopy];
   if ([segue.identifier isEqualToString:@"Show Image"]) {
      [self transferSplitViewBarButtonItemToViewController:segue.destinationViewController];
      if ([sender isKindOfClass:[UITableViewCell class]]) {
         NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
         if (indexPath) {
               if ([segue.destinationViewController respondsToSelector:@selector(setImageURL:)]) {
                  CGRect screenRect = [[UIScreen mainScreen] bounds];
                  CGFloat screenWidth = screenRect.size.width * [[UIScreen mainScreen] scale];
                  CGFloat screenHeight = screenRect.size.height * [[UIScreen mainScreen] scale];
                  NSURL *url;
                  //Flickr large size has 1024 pixels in the maximum dimension. Use 512 in following line to provide lossless 2X zooming.
                  if (screenWidth > 512 || screenHeight > 512) {
                     url = [FlickrFetcher urlForPhoto:self.photos[indexPath.row] format:FlickrPhotoFormatOriginal];
                  } else {
                     url = [FlickrFetcher urlForPhoto:self.photos[indexPath.row] format:FlickrPhotoFormatLarge];
                  }
                  NSMutableDictionary *photoDict = [self.photos[indexPath.row] mutableCopy];
                  [self addPhoto:photoDict toArray:latestPhotos];
                  self.recentPhotos = [latestPhotos copy];
                  // Save the recent photos array to NSUserDefaults
                  [[NSUserDefaults standardUserDefaults] setObject:self.recentPhotos forKey:@"recentPhotos"];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                  [segue.destinationViewController performSelector:@selector(setImageURL:) withObject:url];
                  [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
               }
         }
      }
   }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)refresh
{
   if (self.selectTag) {
      [self loadPhotosWithTag:self.selectTag];
   }
}
- (void)loadPhotosWithTag:(NSString *)selectedTag
{
   self.selectTag = selectedTag;
   [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
   [self.refreshControl beginRefreshing];
   static int networkCounter = 0;
   __block NSArray *stanfordPictures = [[NSArray alloc] init];
   UIApplication *myApplication = [UIApplication sharedApplication];
   dispatch_queue_t q = dispatch_queue_create("stanford table view loading queue", NULL);
   dispatch_async(q, ^{
   // do the work to refresh the table
      networkCounter += 1;
      myApplication.networkActivityIndicatorVisible = YES;
      stanfordPictures = [FlickrFetcher stanfordPhotos];
      networkCounter -= 1;
      if (networkCounter == 0) myApplication.networkActivityIndicatorVisible = NO;
      dispatch_async(dispatch_get_main_queue(), ^{
         //
         NSMutableArray *selectedPictures = [[NSMutableArray alloc] init];
         for (NSMutableDictionary *photo in stanfordPictures) {
            NSString *tags = [photo valueForKey:FLICKR_TAGS];
            NSArray *tagList = [tags componentsSeparatedByString:@" "];
            for (NSString *tag in tagList) {
               NSString *upperTag = [tag capitalizedString];
               if ([upperTag isEqualToString:selectedTag]) {
                  // Found a match. Add it to the array.
                  [selectedPictures addObject:photo];
               }
            }
         }
         //Sort the array
         NSSortDescriptor *byTitle = [[NSSortDescriptor alloc] initWithKey:FLICKR_PHOTO_TITLE ascending:YES];
         NSArray *descriptors = [NSArray arrayWithObjects:byTitle, nil];
         NSArray *sortedTitleList = [selectedPictures sortedArrayUsingDescriptors:descriptors];
         self.photos = [sortedTitleList copy];
         [self.refreshControl endRefreshing];
      });
   });
}

- (NSString *)subtitleForRow:(NSUInteger)row
{
   NSDictionary *description = self.photos[row][@"description"];
   NSString *fullDescription = description[@"_content"];
   return fullDescription;
}
-(void)viewWillAppear:(BOOL)animated
{
   NSMutableArray *latestPhotos = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"recentPhotos"] mutableCopy];
   self.recentPhotos = latestPhotos;
}
@end
