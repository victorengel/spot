//
//  SPoTViewController.m
//  SPoT
//
//  Created by Victor Engel on 3/15/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//
// 1. Access stanfortPhotos method of FlickrFetcher class.
// 2. UI should be UITabBarController based with two tabs, each with a UINavigationController-based stack.
//    Let's call these ALL Photos, and Recent Photos for now.
// 3. Present a listing of all flickr tags with number of photos for each tag. Capitalize tags.
// 4. User selection of a tag causes screen to appear showing a list of titles with subtitles.
// 5. User selects a photo. Present it in a UIScrollView.
// 6. Accommodate resizing of the photo when rotating until user pinches and zooms.
//
// ALL LISTS USE UITableView
//
// 8. Most recents are sorted most recent at top with no duplicates.
// 9. Disclosure indicators are required to navigate furthere.
// 10. Must work properly on iPhone 4 and iPhone 5
//
// Demo starts at 42 minutes into lecture 9.

#import "SPoTViewController.h"
#import "FlickrFetcher.h"

@interface SPoTViewController ()

@end

@implementation SPoTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
   NSArray *stanfordPictures = [FlickrFetcher stanfordPhotos];
   for (NSMutableDictionary *photo in stanfordPictures) {
      NSString *title = [photo valueForKey:FLICKR_PHOTO_TITLE];
      NSLog(@"Title: %@",title);
   }
}


@end
