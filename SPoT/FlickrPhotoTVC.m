//
//  FlickrPhotoTVC.m
//  SPoT
//
//  Created by Victor Engel on 3/20/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "FlickrPhotoTVC.h"
#import "FlickrFetcher.h"

@interface FlickrPhotoTVC () /*<UISplitViewControllerDelegate>*/
// Added <UISplitViewControllerDelegate> for handling split view controller in iPad storyboard.

@end

@implementation FlickrPhotoTVC

-(void)setPhotos:(NSArray *)photos
{
   _photos = photos;
   [self.tableView reloadData];
}

-(NSMutableArray *)recentPhotos
{
   if (!_recentPhotos) {
      _recentPhotos = [[NSMutableArray alloc] init];
   }
   return _recentPhotos;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   NSMutableArray *latestPhotos = [self.recentPhotos mutableCopy];
   if ([sender isKindOfClass:[UITableViewCell class]]) {
      NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
      if (indexPath) {
         if ([segue.identifier isEqualToString:@"Show Image"]) {
            if ([segue.destinationViewController respondsToSelector:@selector(setImageURL:)]) {
               // Determine which URL to get based upon screen size.
               CGRect screenRect = [[UIScreen mainScreen] bounds];
               CGFloat screenWidth = screenRect.size.width * [[UIScreen mainScreen] scale];
               CGFloat screenHeight = screenRect.size.height * [[UIScreen mainScreen] scale];
               NSURL *url;
               //Flickr large size has 1024 pixels in the maximum dimension. Use 512 in following line to provide lossless 2X zooming.
               if (screenWidth > 512 || screenHeight > 512) {
                  url = [FlickrFetcher urlForPhoto:self.photos[indexPath.row] format:FlickrPhotoFormatOriginal];
               } else {
                  url = [FlickrFetcher urlForPhoto:self.photos[indexPath.row] format:FlickrPhotoFormatLarge];
               }
               NSMutableDictionary *photoDict = [self.photos[indexPath.row] mutableCopy];
               [self addPhoto:photoDict toArray:latestPhotos];
               self.recentPhotos = [latestPhotos copy];
               // Save the recent photos array to NSUserDefaults
               [[NSUserDefaults standardUserDefaults] setObject:self.recentPhotos forKey:@"recentPhotos"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               [segue.destinationViewController performSelector:@selector(setImageURL:) withObject:url];
               [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
            }
         }
      }
   }
}

-(void)addPhoto:(NSDictionary *)photoDict toArray:(NSMutableArray *)photoArray
{
   //Check if photo is already in array. If so, remove it. Then add it to beginning.
   [photoArray removeObject:photoDict];
   [photoArray insertObject:photoDict atIndex:0];
   //clear cache
   for (int index=[photoArray count]-1; index>5; index--) {
      //Remove cached data, if exists
      CGRect screenRect = [[UIScreen mainScreen] bounds];
      CGFloat screenWidth = screenRect.size.width;
      CGFloat screenHeight = screenRect.size.height;
      NSURL *url;
      if (screenWidth > 1024 || screenHeight > 768) {
         url = [FlickrFetcher urlForPhoto:photoArray[index] format:FlickrPhotoFormatOriginal];
      } else {
         url = [FlickrFetcher urlForPhoto:photoArray[index] format:FlickrPhotoFormatLarge];
      }
      [self removeFromCache:url];
   }
}

-(void) removeFromCache:(NSURL *)url
{
   NSFileManager *fileManager = [[NSFileManager alloc] init];
   NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
   NSURL *cachedURL = urls[0];
   //Manually add a cache directory.
   cachedURL = [cachedURL URLByAppendingPathComponent:@"cache"];
   NSArray *passedPathComponents = [url pathComponents];
   for (NSString *pathComponent in passedPathComponents) {
      cachedURL = [cachedURL URLByAppendingPathComponent:pathComponent];
   }
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.photos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *CellIdentifier = @"Flickr Photo";
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
   cell.textLabel.text = [self titleForRow:indexPath.row];
   cell.detailTextLabel.text = [self subtitleForRow:indexPath.row];
    
   return cell;
}

- (NSString *)titleForRow:(NSUInteger)row
{
   return [self.photos[row][FLICKR_PHOTO_TITLE] description];
}

- (NSString *)subtitleForRow:(NSUInteger)row
{
   return [self.photos[row][FLICKR_PHOTO_OWNER] description];
}

@end

