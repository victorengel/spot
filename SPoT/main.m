//
//  main.m
//  SPoT
//
//  Created by Victor Engel on 3/15/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPoTAppDelegate.h"

int main(int argc, char *argv[])
{
   @autoreleasepool {
       return UIApplicationMain(argc, argv, nil, NSStringFromClass([SPoTAppDelegate class]));
   }
}
