//
//  FlickrPhotoTVC.h
//  SPoT
//
//  Created by Victor Engel on 3/20/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoTVC : UITableViewController

@property (nonatomic, strong) NSArray *photos; // of NSDictionary
@property (nonatomic, strong) NSMutableArray *recentPhotos; // same as photos but with timestamp added
-(void)addPhoto:(NSDictionary *)photoDict toArray:(NSMutableArray *)photoArray;
- (NSString *)titleForRow:(NSUInteger)row;

@end
