//
//  TabBarController.m
//  SPoT
//
//  Created by Victor Engel on 3/31/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "TabBarController.h"
#import "IViewController.h"

@interface TabBarController ()<UISplitViewControllerDelegate>                        //001

@end

@implementation TabBarController

#pragma mark - UISplitViewControllerDelegate

-(void)awakeFromNib
{
   [super awakeFromNib];                                                             
   self.splitViewController.delegate = self;                                         //002
}

//Following method decides whether the master view appears or not in portrait orientation.
-(BOOL)splitViewController:(UISplitViewController *)svc
  shouldHideViewController:(UIViewController *)vc
             inOrientation:(UIInterfaceOrientation)orientation
{
   //Default behavior: hide in portrait only.
   return UIInterfaceOrientationIsPortrait(orientation);
}

                                                                                     //003
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
   barButtonItem.title = @"List";
   id detailViewController = [self.splitViewController.viewControllers lastObject]; //get a handle to the detail VC
   [detailViewController setSplitViewBarButtonItem:barButtonItem];
   // The detail view controller must handle this method, which must be public, if the master is the delegate so it can reference it.
}

                                                                                     //004
-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
   id detailViewController = [self.splitViewController.viewControllers lastObject];
   [detailViewController setSplitViewBarButtonItem:nil];
}

                                                                                     //006
                                                                                     //See StanfordTVC

@end
