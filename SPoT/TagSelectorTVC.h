//
//  TagSelectorTVC.h
//  SPoT
//
//  Created by Victor Engel on 3/21/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "FlickrPhotoTVC.h"

@interface TagSelectorTVC : FlickrPhotoTVC

@property (strong, nonatomic) NSArray *tags; //of NSDictionary
/* Each NSDictionary has the following keys:
 tag - the flickr tag being tracked
 counter - number of items having this flickr tag
 */

@end
