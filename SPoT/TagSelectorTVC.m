//
//  TagSelectorTVC.m
//  SPoT
//
//  Created by Victor Engel on 3/21/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "TagSelectorTVC.h"
#import "FlickrFetcher.h"

//@interface TagSelectorTVC () <UISplitViewControllerDelegate>
@interface TagSelectorTVC () 

@end

@implementation TagSelectorTVC



#pragma mark - Setters/Getters

-(void)setTags:(NSArray *)tags
{
   _tags = tags;
   [self.tableView reloadData];
}

#pragma mark - Segue

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   if ([sender isKindOfClass:[UITableViewCell class]]) {
      NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
      if (indexPath) {
         if ([segue.identifier isEqualToString:@"Show Tagged List"]) {
            if ([segue.destinationViewController respondsToSelector:@selector(loadPhotosWithTag:)]) {
               NSString *tag = self.tags[indexPath.row][@"tag"];
               //NSLog(@"About to perform selector loadPhotosWithTag: %@",tag);
               [segue.destinationViewController performSelector:@selector(loadPhotosWithTag:) withObject:tag];
               //NSURL *url = [FlickrFetcher urlForPhoto:self.photos[indexPath.row] format:FlickrPhotoFormatLarge];
               //[segue.destinationViewController performSelector:@selector(setImageURL:) withObject:url];
               //[segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
            }
         }
      }
   }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)refresh
{
   // called by the silly putty
   [self.refreshControl beginRefreshing];
   dispatch_queue_t q = dispatch_queue_create("table view loading queue", NULL);
   dispatch_async(q, ^{
      // do the work to refresh the table
      [self.tableView reloadData];
      dispatch_async(dispatch_get_main_queue(), ^{
         [self.refreshControl endRefreshing];
      });
   });
}

- (void)viewDidLoad
{
   [super viewDidLoad];
   // Add the silly putty refresh control.
   [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
   NSMutableArray *tagList = [[NSMutableArray alloc] init];//tags dictionaries (tags with their count)
                                                                     //Gather a listing of tags here.
   UIApplication *myApplication = [UIApplication sharedApplication];
   static int networkCounter = 0;
   networkCounter += 1;
   [myApplication setNetworkActivityIndicatorVisible:YES];
   NSArray *stanfordPictures = [FlickrFetcher stanfordPhotos];
   networkCounter -= 1;
   if (networkCounter == 0) [myApplication setNetworkActivityIndicatorVisible:NO];
   for (NSMutableDictionary *photo in stanfordPictures) {
      NSString *tags = [photo valueForKey:FLICKR_TAGS];
      NSArray *pictureTags = [tags componentsSeparatedByString:@" "];
      for (NSString *tag in pictureTags) {
         if (![tag isEqualToString:@"cs193pspot"] && ![tag isEqualToString:@"landscape"] && ![tag isEqualToString:@"portrait"]) {
            NSString *upperTag = [tag capitalizedString];
            [self updateCountForTag:upperTag usingArray:tagList];
         }
      }
   }
   //Sort the array
   NSSortDescriptor *byTag = [[NSSortDescriptor alloc] initWithKey:@"tag" ascending:YES];
   NSArray *descriptors = [NSArray arrayWithObjects:byTag, nil];
   NSArray *sortedTagList = [tagList sortedArrayUsingDescriptors:descriptors];
   self.tags = [sortedTagList copy];
}

- (void)updateCountForTag: (NSString *)tag usingArray:(NSMutableArray *)taglist
{
   BOOL counterUpdated = NO;
   for (NSMutableDictionary *tagDict in taglist) {
      //Check if this array item matches the tag. If so, update the counter.
      NSString *arrayTag = [tagDict valueForKey:@"tag"];
      if ([arrayTag isEqualToString:tag]) {
         NSNumber *arrayCounter = [tagDict valueForKey:@"counter"];
         arrayCounter = @([arrayCounter integerValue] + 1);
         [tagDict setValue:arrayCounter forKey:@"counter"];
         counterUpdated = YES;
         break;
      }
   }
   if (!counterUpdated) {
      //An existing item was not updated, so add a new one
      NSMutableDictionary *tagDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:tag,@"tag",@(1),@"counter", nil];
      [taglist addObject:tagDict];
   }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [self.tags count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *CellIdentifier = @"Flickr Tag";
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
   
   // Configure the cell...
   cell.textLabel.text = [self titleForRow:indexPath.row];
   cell.detailTextLabel.text = [self counterForRow:indexPath.row];
   
   return cell;
}

- (NSString *)titleForRow:(NSUInteger)row
{
   return [self.tags[row][@"tag"] description];
}

- (NSString *)counterForRow:(NSUInteger)row
{
   return [self.tags[row][@"counter"] description];
}

@end
