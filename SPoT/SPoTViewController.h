//
//  SPoTViewController.h
//  SPoT
//
//  Created by Victor Engel on 3/15/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPoTViewController : UIViewController

@property (nonatomic, strong) NSURL *imageURL;

@end
